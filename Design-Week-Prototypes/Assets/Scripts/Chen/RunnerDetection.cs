﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;

public class RunnerDetection : MonoBehaviour
{
    Blackboard bb;
    DynamicList Runner;
    public float searchTime;

    void Awake()
    {
        bb = GetComponent<Blackboard>();
        Runner = bb.GetDynamicList("Runner");
    }

    void OnVisionEnter(Collider other)
    {
        if (other.tag == "Runner" && !Runner.Contains(other.gameObject))
        {
            Runner.Add(other.gameObject);
            bb.SendEvent("RunnerDetected");
            CancelInvoke();
        }
    }

    void OnVisionExit(Collider other)
    {
        if (other.tag == "Runner" && Runner.Contains(other.gameObject))
        {
            Runner.Remove(other.gameObject);
            Invoke("OnRunnerLost", searchTime);
        }
    }

    void OnRunnerLost()
    {
        if (Runner.Count == 0)
        {
            bb.SendEvent("RunnerLost");
        }
    }
}