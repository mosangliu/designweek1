﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;

public class Ghost : MonoBehaviour
{
    public float health;
    Blackboard bb;

    // Start is called before the first frame update
    void Start()
    {
        bb = GetComponent<Blackboard>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTorched()
    {
        bb.SendEvent("Torched");
    }
    public void OnTorchCancelled()
    {
        bb.SendEvent("TorchCancelled");
    }
}
