﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnGhost : MonoBehaviour
{
    public int ghostNumber;
    bool ableToSpawn = true;
    public GameObject ghostPrefab;
    private BoxCollider spawnZone;
    private BoxCollider wanderZone;

    private void Start()
    {
        spawnZone = transform.Find("GhostSpawnZone").GetComponent<BoxCollider>();
        wanderZone = transform.Find("GhostWanderZone").GetComponent<BoxCollider>();
    }

    public void Spawn()
    {
        if (ableToSpawn)
        {
            for (int i = 0; i < ghostNumber; i++)
            {
                Vector3 spawnPosition = GenerateRandomSpawnPosition(spawnZone.bounds);
                GameObject ghost = Instantiate(ghostPrefab, spawnPosition, Quaternion.identity);
                ghost.GetComponent<GhostWandering>().wanderZone = wanderZone;
                ghost.transform.parent = GameObject.Find("Ghosts").transform;
            }
            ableToSpawn = false;
        }
    }

    public Vector3 GenerateRandomSpawnPosition(Bounds bounds)
    {
        Vector3 randomPositionOnPlane = new Vector3(
            Random.Range(bounds.min.x, bounds.max.x),
            0,
            Random.Range(bounds.min.z, bounds.max.z)
        );
        return randomPositionOnPlane;
    }
}
