﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectDetection : MonoBehaviour
{
    public List<GameObject> visibleObjects;
    public GameObject visionOwner;

    public virtual void OnTriggerEnter(Collider other)
    {
        if (visibleObjects.Contains(other.gameObject)) return;

        visibleObjects.Add(other.gameObject);
        visionOwner.SendMessage("OnVisionEnter", other, SendMessageOptions.DontRequireReceiver);
    }

    void OnTriggerExit(Collider other)
    {
        if (visibleObjects.Contains(other.gameObject))
        {
            visibleObjects.Remove(other.gameObject);
        }
        visionOwner.SendMessage("OnVisionExit", other, SendMessageOptions.DontRequireReceiver);
    }

}
