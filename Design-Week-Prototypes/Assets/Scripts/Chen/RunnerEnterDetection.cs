﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunnerEnterDetection : MonoBehaviour
{
    GameObject spawner;

    private void Start()
    {
        spawner = transform.parent.gameObject;
    }

    //Spawn on zone enter.
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Runner")
        {
            spawner.SendMessage("Spawn", SendMessageOptions.DontRequireReceiver);
        }
    }
}
