﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    private Transform runner;
    private Vector3 targetPosition;
    private Vector3 originalPosition;
    [SerializeField]
    float offsetZ;
    //smooth damp
    public float smoothTime = 0.3F;
    private Vector3 velocity = Vector3.zero;
    //private float 
    void Awake()
    {
        runner = FindObjectOfType<Runner>().transform;
    }

    private void Start()
    {
        originalPosition = transform.position;
    }
    // Update is called once per frame
    void Update()
    {
        targetPosition = runner.position;
        targetPosition = new Vector3(runner.position.x, originalPosition.y, runner.position.z - offsetZ);
        transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime, Mathf.Infinity, Time.deltaTime);
    }
}
