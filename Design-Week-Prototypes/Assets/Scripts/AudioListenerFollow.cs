﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioListenerFollow : MonoBehaviour
{
    private Transform runner;
    // Start is called before the first frame update
    void Start()
    {
        runner = FindObjectOfType<Runner>().transform;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = runner.position;
    }
}
