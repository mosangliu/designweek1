﻿using UnityEngine;

public class Runner : MonoBehaviour
{
    private Camera m_camera;

    Animator runnerAnimator;

    bool isWalking;
    bool isTurning;

    public float maxHealth;
    [SerializeField]
    float currentHealth;
    [SerializeField]
    Transform healthIndicator;
    [SerializeField]
    bool isAlive = true;

    // controller
    [SerializeField]
    float maxSpeed;
    [SerializeField]
    float accelerationTime;
    [SerializeField]
    float decelerationTime;
    private float currentSpeed = 0;
    private Vector3 direction;
    // direction space
    [SerializeField]
    Transform directionSpace;
    private void Awake()
    {
        m_camera = FindObjectOfType<Camera>();
        runnerAnimator = transform.GetChild(0).GetComponent<Animator>();
    }
    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        PlayerMovement(new Vector2(Input.GetAxis("Vertical-Movement"), 0),
            new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")));

        runnerAnimator.SetBool("isWalking", isWalking || isTurning);
    }

    private void PlayerMovement(Vector2 movementInput, Vector2 lookInput)
    {
        if (movementInput != Vector2.zero)
        {
            isWalking = true;

            // acceleration by movement input
            direction = directionSpace.TransformDirection(Vector3.forward * movementInput.x).normalized;
            float acceleration = movementInput.magnitude == 0 ? -maxSpeed / decelerationTime : maxSpeed / accelerationTime;
            currentSpeed += acceleration * Time.deltaTime;
            currentSpeed = Mathf.Clamp(currentSpeed, 0f, maxSpeed);
            transform.Translate(direction * currentSpeed * Time.deltaTime, Space.World);
        }
        else
        {
            isWalking = false;
        }
        // Look towards rocker direction
        if (lookInput.magnitude != 0)
        {
            isTurning = true;

            Vector3 lookDirection = new Vector3(lookInput.x, 0, lookInput.y);
            transform.rotation = Quaternion.RotateTowards(this.transform.rotation, Quaternion.LookRotation(lookDirection), 8f);
        }
        else
        {
            isTurning = false;
        }
        
    }

    public void OnAttackedByGhost()
    {
        currentHealth--;
        HealthIndicator();
        if (currentHealth <= 0)
        {
            OnDeath();
        }
    }

    void HealthIndicator()
    {
        for (int i = 0; i < healthIndicator.childCount; i++)
        {
            if(i >= currentHealth)
            {
                healthIndicator.GetChild(i).GetChild(0).gameObject.SetActive(false);
            }
        }
    }

    void OnDeath()
    {
        isAlive = false;
        Time.timeScale = 0;
    }
}
