﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;
using UnityEngine.AI;

public class GhostChasing : StateBehaviour
{
    public float catchDistance;
    private NavMeshAgent agent;
    Animator ghostAnimation;
    GameObject runner;

    private Material ghostMat;
    private float transparency = 1;
    // Called when the state is enabled
    void OnEnable () {
		Debug.Log("Started *GhostChasing*");

        ghostAnimation = GetComponent<Animator>();
        ghostAnimation.Play("Armature|Floating");
        agent = GetComponent<NavMeshAgent>();
        agent.isStopped = false;
        runner = blackboard.GetDynamicList("Runner")[0] as GameObject;
        InvokeRepeating("RefreshTargetPosition", 0, 1);

        ghostMat = transform.Find("Ghost").Find("Cube").GetComponent<SkinnedMeshRenderer>().materials[0];
        ghostMat.SetFloat("_Transparency", transparency);
    }
 
	// Called when the state is disabled
	void OnDisable () {
		Debug.Log("Stopped *GhostChasing*");

        CancelInvoke();
	}
	
	// Update is called once per frame
	void Update () {
        CheckTargetDistance();
    }

    void RefreshTargetPosition()
    {
        agent.SetDestination(runner.transform.position);
    }

    void CheckTargetDistance()
    {
        if (Vector3.Magnitude(runner.transform.position - transform.position) <= catchDistance)
        {
            Debug.Log("gg");
            runner.SendMessage("OnAttackedByGhost", SendMessageOptions.DontRequireReceiver);
            blackboard.SendEvent("GotRunner");
        }
    }
}


