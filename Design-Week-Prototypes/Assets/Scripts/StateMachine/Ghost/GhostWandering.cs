﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;
using UnityEngine.AI;

public class GhostWandering : StateBehaviour
{
    public float wanderTimer;
    public BoxCollider wanderZone;
    private NavMeshAgent agent;
    private float timer;
    Animator ghostAnimation;

    private Material ghostMat;
    private float transparency = 1;
    [SerializeField]
    float flickeringFrequency;
    // Called when the state is enabled
    void OnEnable () {
		Debug.Log("Started *GhostWandering*");

        ghostAnimation = GetComponent<Animator>();
        ghostAnimation.Play("Armature|Floating");
        agent = GetComponent<NavMeshAgent>();
        agent.isStopped = false;
        timer = wanderTimer;

        ghostMat = transform.Find("Ghost").Find("Cube").GetComponent<SkinnedMeshRenderer>().materials[0];
    }
 
	// Called when the state is disabled
	void OnDisable () {
		Debug.Log("Stopped *GhostWandering*");
	}
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;

        if (timer >= wanderTimer)
        {
            Vector3 newPosition = GenerateRandomTargetPosition(wanderZone.bounds);
            agent.SetDestination(newPosition);
            timer = 0;
        }

        transparency = Mathf.Sin(Time.time * flickeringFrequency);
        transparency = Mathf.Clamp(transparency, 0, 1);
        ghostMat.SetFloat("_Transparency", transparency);
        print(transparency);
    }

    public Vector3 GenerateRandomTargetPosition(Bounds bounds)
    {
        Vector3 randomPositionOnPlane = new Vector3(
            Random.Range(bounds.min.x, bounds.max.x),
            0,
            Random.Range(bounds.min.z, bounds.max.z)
        );
        NavMeshHit navHit;
        NavMesh.SamplePosition(randomPositionOnPlane, out navHit, 10, NavMesh.AllAreas);
        return navHit.position;
    }
}


