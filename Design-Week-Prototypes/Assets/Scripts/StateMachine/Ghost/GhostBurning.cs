﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;
using UnityEngine.AI;

public class GhostBurning : StateBehaviour
{
    public float burnTimer;
    private float timer;
    private NavMeshAgent agent;
    Animator ghostAnimation;
    private Transform runner;
    [SerializeField]
    Material ghostMat;
    private float glow = 2;
    private float transparency = 1;
    //smooth damp
    private float velocity = 0f;
    // Called when the state is enabled
    void OnEnable () {
		Debug.Log("Started *GhostBurning*");

        ghostAnimation = GetComponent<Animator>();
        ghostAnimation.Play("Armature|Burn");
        agent = GetComponent<NavMeshAgent>();
        agent.isStopped = true;
        timer = 0;

        runner = GameObject.Find("Runner").transform;

        ghostMat = transform.Find("Ghost").Find("Cube").GetComponent<SkinnedMeshRenderer>().materials[0];
        ghostMat.SetFloat("_Transparency", transparency);
    }

    // Called when the state is disabled
    void OnDisable () {
		Debug.Log("Stopped *GhostBurning*");
	}
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;

        if (timer >= burnTimer)
        {
            blackboard.SendEvent("BurnedOut");
            timer = 0;
        }
        transform.rotation = Quaternion.RotateTowards(this.transform.rotation, Quaternion.LookRotation(runner.position - transform.position), 5f);

        glow = Mathf.Lerp(glow, 10, Time.deltaTime * .5f);
        ghostMat.SetFloat("_Glow", glow);
    }
}


