﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;
using UnityEngine.AI;

public class GhostChaseConsidering : StateBehaviour
{
    [Range(0, 10)]
    public int chanceOfChase;

    // Called when the state is enabled
    void OnEnable () {
		Debug.Log("Started *GhostChaseConsidering*");

        GetComponent<NavMeshAgent>().isStopped = true;

        if (Random.Range(1, 11) <= chanceOfChase)
        {
            SendEvent("ChaseRunnerDecided");
        }
        else
        {
            SendEvent("NotChaseRunnerDecided");
        }
    }
 
	// Called when the state is disabled
	void OnDisable () {
		Debug.Log("Stopped *GhostChaseConsidering*");
	}
	
	// Update is called once per frame
	void Update () {

    }
}


