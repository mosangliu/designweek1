﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;
using UnityEngine.AI;

public class GhostAttacking : StateBehaviour
{
    public float attackTimer;
    private float timer;

    private NavMeshAgent agent;
    Animator ghostAnimation;

    private Material ghostMat;
    private float transparency = 1;
    // Called when the state is enabled
    void OnEnable()
    {
        Debug.Log("Started *GhostAttacking*");

        ghostAnimation = GetComponent<Animator>();
        ghostAnimation.Play("Armature|Scaring");
        agent = GetComponent<NavMeshAgent>();
        agent.isStopped = true;
        timer = 0;

        ghostMat = transform.Find("Ghost").Find("Cube").GetComponent<SkinnedMeshRenderer>().materials[0];
        ghostMat.SetFloat("_Transparency", transparency);
    }

    // Called when the state is disabled
    void OnDisable()
    {
        Debug.Log("Stopped *GhostAttacking*");
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if (timer >= attackTimer)
        {
            blackboard.SendEvent("Attacked");
            Destroy(gameObject);
            timer = 0;
        }
    }
}