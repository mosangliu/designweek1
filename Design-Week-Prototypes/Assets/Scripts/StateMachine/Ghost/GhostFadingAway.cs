﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;
using UnityEngine.AI;

public class GhostFadingAway : StateBehaviour
{
    public float fadeTimer;
    private float timer;
    private NavMeshAgent agent;
    Animator ghostAnimation;

    private Material ghostMat;
    private float transparency = 1;
    // Called when the state is enabled
    void OnEnable()
    {
        Debug.Log("Started *GhostFadingAway*");

        ghostAnimation = GetComponent<Animator>();
        ghostAnimation.Play("Armature|Fade Away");
        agent = GetComponent<NavMeshAgent>();
        agent.isStopped = true;
        timer = 0;

        ghostMat = transform.Find("Ghost").Find("Cube").GetComponent<SkinnedMeshRenderer>().materials[0];
        ghostMat.SetFloat("_Transparency", transparency);
    }

    // Called when the state is disabled
    void OnDisable()
    {
        Debug.Log("Stopped *GhostFadingAway*");
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if (timer >= fadeTimer)
        {
            Destroy(gameObject);
        }

        transform.Translate(-transform.forward * Time.deltaTime * 4, Space.World);

        transparency = Mathf.Lerp(transparency, 0, Time.deltaTime * 2f);
        ghostMat.SetFloat("_Transparency", transparency);
    }
}