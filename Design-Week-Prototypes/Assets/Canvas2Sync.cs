﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Canvas2Sync : MonoBehaviour
{
    [SerializeField]
    Transform canvas1;
    private Image c1_flashlightIndicator;
    private Image c2_flashlightIndicator;

    Transform c1_healthIndicator;
    private Transform[] c1_healthIndicators;
    private Transform[] c2_healthIndicators;
    private void Awake()
    {
        c1_flashlightIndicator = canvas1.GetChild(0).GetComponent<Image>();
        c2_flashlightIndicator = transform.GetChild(0).GetComponent<Image>();
        c1_healthIndicator = canvas1.GetChild(1);
    }
    // Start is called before the first frame update
    void Start()
    {
        c1_healthIndicators = new Transform[c1_healthIndicator.childCount];
        c2_healthIndicators = new Transform[c1_healthIndicator.childCount];
        for (int i = 0; i < c1_healthIndicator.childCount; i++)
        {
            c1_healthIndicators[i] = c1_healthIndicator.GetChild(i).GetChild(0);
            c2_healthIndicators[i] = transform.GetChild(1).GetChild(i).GetChild(0);
        }

    }

    // Update is called once per frame
    void Update()
    {
        c2_flashlightIndicator.fillAmount = c1_flashlightIndicator.fillAmount;
        c2_flashlightIndicator.color = c1_flashlightIndicator.color;

        for (int i = 0; i < c1_healthIndicator.childCount; i++)
        {
            c2_healthIndicators[i].gameObject.SetActive(c1_healthIndicators[i].gameObject.activeInHierarchy);
        }
    }
}
