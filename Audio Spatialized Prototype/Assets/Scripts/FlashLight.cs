using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlashLight : MonoBehaviour
{
    [SerializeField]
    GameObject flashLight;
    private float lightAngle;
    private float lightRange;
    [SerializeField]
    float batteryLife;
    [SerializeField]
    Image batteryIndicator;
    [SerializeField]
    Color fullCharged;
    [SerializeField]
    Color runningOut;

    [SerializeField]
    Transform ghostParent;

    private float currentBattery = 1;
    // Start is called before the first frame update
    void Start()
    {
        lightAngle = flashLight.GetComponent<Light>().spotAngle;
        lightRange = flashLight.GetComponent<Light>().range;
    }

    // Update is called once per frame
    void Update()
    {
        Light();
        foreach (Transform ghost in ghostParent)
        {
            GoastDetection(ghost);
        }
    }
    void Light()
    {
        if (Input.GetButtonDown("FlashLight"))
        {
            flashLight.SetActive(!flashLight.activeInHierarchy);
        }

        if (flashLight.activeInHierarchy)
            currentBattery -= Time.deltaTime / batteryLife;
        if (!flashLight.activeInHierarchy)
            currentBattery += Time.deltaTime / batteryLife;

        currentBattery = Mathf.Clamp(currentBattery, 0, 1);
        if (currentBattery == 0)
        {
            flashLight.SetActive(!flashLight.activeInHierarchy);
        }
        // Indicator
        batteryIndicator.fillAmount = currentBattery;
        batteryIndicator.color = currentBattery > .4f ? fullCharged : runningOut;
    }

    void GoastDetection(Transform tempGhost)
    {
        Vector2 vectorToGhost = new Vector2(tempGhost.position.x - transform.position.x, tempGhost.position.z - transform.position.z);
        float angle = Vector2.Angle(new Vector2(transform.forward.x, transform.forward.z), vectorToGhost);
        if(flashLight.activeInHierarchy && angle < lightAngle/2 && vectorToGhost.magnitude < lightRange)
        {
            
        }
    }
}
