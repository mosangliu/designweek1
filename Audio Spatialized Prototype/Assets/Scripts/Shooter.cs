﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    private Vector3 m_vectorToPlayer;
    private ChargeShoot m_chargeshoot;

    private void Awake()
    {
        m_chargeshoot = gameObject.GetComponent<ChargeShoot>();
    }
    // Update is called once per frame
    void Update()
    {
        AimToMouse();
        Charge();
    }

    void AimToMouse()
    {
        m_vectorToPlayer = Input.mousePosition - Camera.main.WorldToScreenPoint(transform.position);
        transform.rotation = Quaternion.AngleAxis(-Mathf.Atan2(m_vectorToPlayer.y, m_vectorToPlayer.x) * Mathf.Rad2Deg + 90, Vector3.up);
    }

    void Charge()
    {
        if (Input.GetButtonDown("Jump"))
        {
            m_chargeshoot.StartCharge();
        }

        if (Input.GetButtonUp("Jump"))
        {
            StartCoroutine(m_chargeshoot.Launch());
        }
    }
}

