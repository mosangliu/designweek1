using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectionSpace : MonoBehaviour
{
    [SerializeField]
    Vector2 waitTimeRange;
    private float waitPeriod;
    [SerializeField]
    Vector2 rotationSpeedRange;
    private float rotateSpeedInDeg;
    private Vector2 direction;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(ChangeDirectionAfterDelay());
    }

    IEnumerator ChangeDirectionAfterDelay()
    {
        waitPeriod = Random.Range(waitTimeRange.x, waitTimeRange.y);
        yield return new WaitForSeconds(waitPeriod);
        direction = Random.insideUnitCircle;
        direction = new Vector3(direction.x, 0, direction.y);
        StartCoroutine(ChangeDirectionAfterDelay());
    }

    private void Update()
    {
        rotateSpeedInDeg = Random.Range(rotationSpeedRange.x, rotationSpeedRange.y);
        transform.rotation = Quaternion.RotateTowards(this.transform.rotation, Quaternion.LookRotation(direction), rotateSpeedInDeg * Time.deltaTime);
    }
}
