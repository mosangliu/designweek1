using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ghost : MonoBehaviour
{
    private Transform runnerTransform;

    [SerializeField]
    private float rotateSpeedInDeg;
    [SerializeField]
    private float forwardSpeed;
    private void Awake()
    {
        runnerTransform = FindObjectOfType<Runner>().transform;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Chasing();
    }

    void Chasing()
    {
        Vector3 vectorToRunner = runnerTransform.position - transform.position;
        float directionSignX = -1 * Mathf.Sign(Vector3.Dot(new Vector3(0, vectorToRunner.y, vectorToRunner.z), transform.up));
        float directionSignY = -1 * Mathf.Sign(Vector3.Dot(new Vector3(vectorToRunner.x, 0, vectorToRunner.z), transform.right));

        // Moving forward
        transform.Translate(transform.forward * forwardSpeed * Time.deltaTime, Space.World);

        transform.rotation = Quaternion.RotateTowards(this.transform.rotation, Quaternion.LookRotation(vectorToRunner), rotateSpeedInDeg * Time.deltaTime);
    }
}
