using UnityEngine;

public class Runner : MonoBehaviour
{
    private Camera m_camera;
    // controller
    [SerializeField]
    float maxSpeed;
    [SerializeField]
    float accelerationTime;
    [SerializeField]
    float decelerationTime;
    private float currentSpeed = 0;
    private Vector3 direction;
    // direction space
    [SerializeField]
    Transform directionSpace;
    private void Awake()
    {
        m_camera = FindObjectOfType<Camera>();
    }
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        PlayerMovement(new Vector2(Input.GetAxis("Vertical-Movement"), 0), 
            new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")));
    }

    private void PlayerMovement(Vector2 movementInput, Vector2 lookInput)
    {
        // acceleration by movement input
        direction = directionSpace.TransformDirection(Vector3.forward * movementInput.x).normalized;
        float acceleration = movementInput.magnitude == 0 ? -maxSpeed / decelerationTime : maxSpeed / accelerationTime;
        currentSpeed += acceleration * Time.deltaTime;
        currentSpeed = Mathf.Clamp(currentSpeed, 0f, maxSpeed);
        transform.Translate(direction * currentSpeed * Time.deltaTime, Space.World);

        // Look towards rocker direction
        if (lookInput.magnitude != 0)
        {
            Vector3 lookDirection = new Vector3(lookInput.x, 0, lookInput.y);
            transform.rotation = Quaternion.RotateTowards(this.transform.rotation, Quaternion.LookRotation(lookDirection), 8f);
        }
    }
}
