﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoastSpawner : MonoBehaviour
{
    [SerializeField]
    Vector3 gizmoSize;
    private void OnDrawGizmos()
    {
        Gizmos.color = new Color(1, 0, 0, .2f);
        Gizmos.DrawCube(transform.position, gizmoSize);
    }
}
