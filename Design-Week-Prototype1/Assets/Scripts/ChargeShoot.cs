﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChargeShoot : MonoBehaviour
{
    [SerializeField]
    private Transform bulletParent;

    private Shooter shooter;
    private bool isCharging;
    private float timer;
    private float pressTime;

    [SerializeField]
    private float maxCharge = 1.5f;
    [SerializeField]
    private float minForce = 500;
    [SerializeField]
    private float maxForce = 2000;
    [SerializeField]
    private float shoot = 45;
    // Start is called before the first frame update

    public void StartCharge()
    {
        isCharging = true;
        timer = Time.time;
    }
    /// <summary>
    /// Destroy current joint and add an impulse force to player
    /// </summary>
    public IEnumerator Launch()
    {
        isCharging = false;
        pressTime = Time.time - timer;

        // add force based on press time
        float force = minForce + pressTime / maxCharge * (maxForce - minForce);
        force = Mathf.Clamp(force, minForce, maxForce);
        foreach (Transform bullet in bulletParent)
        {
            // force direction aiming to view direction with a jumpAngle
            Vector3 forceDirection = new Vector3(transform.forward.x, Mathf.Tan(shoot * Mathf.Deg2Rad), transform.forward.z).normalized;
            bullet.GetComponent<Rigidbody>().AddForce(forceDirection * force, ForceMode.Impulse);

            bullet.transform.position = transform.position;
            yield return new WaitForSeconds(.2f);
        }
    }
}
